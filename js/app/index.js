var
    config = {
        base_url: 'http://thefrontpage.herokuapp.com/sources',
        sources: [
            {
                name: 'Hacker News',
                url: 'hackernews'
            },
            {
                name: 'Reddit',
                url: 'reddit'
            },
            {
                name: 'Product Hunt',
                url: 'producthunt'
            },
            {
                name: 'r/programming',
                url: 'rprogramming'
            }
        ]
    },

    helpers = {
        get_previous: function(array, index) {
            return (array.length + index-1)%array.length;
        },
        get_next: function(array, index) {
            return (index+1)%array.length;
        },
        open_in_new_tab: function(event) {
            event.original.preventDefault();
            var node = $(event.node);
            node.attr('target', '_blank');
            window.open(node.attr('href'));
        }
    },

    model_helpers = {
        parse_images: function(ractive, feeds) {
            _.map(feeds, function(feed) {
                _.chain(feed.stories)
                .filter(function(story) {
                    var url = story.title.href;
                    return story.image === undefined && (url.split('jpg').length > 1 || url.split('gif').length > 1 || url.split('imgur').length > 1);
                })
                .map(function(story){
                    var
                        format = '',
                        arr = story.title.href.split('.'),
                        suffix = arr[arr.length-1];
                    if (suffix.split('/').length === 1) {
                        format = suffix;
                    }
                    story.image = {
                        src: story.title.href,
                        format: format
                    };
                });
            ractive.update();
            });
        },

        fetch_feeds: function(ractive, source) {
            var
                url = [config.base_url, source.url].join('/'),
                that = this;
            $.get(url)
            .done(function(data) {
                source.feeds = data.feeds;
                source.current_feed = 0;
                ractive.update();
                that.parse_images(ractive, source.feeds);
            });
        },

        fetch_sources: function(ractive) {
            var
                data = ractive.data,
                that = this;
            _.chain([helpers.get_previous(data.sources,data.current_source),   data.current_source,
                helpers.get_next(data.sources,data.current_source)])
            .filter(function(index) {
                return data.sources[index].feeds === undefined;
            })
            .map(function(index) {
                that.fetch_feeds(ractive, ractive.data.sources[index]);
            });
        }
    },

    event_handlers = {
        previous: function(ractive) {
            var data = ractive.data;
            data.current_source = helpers.get_previous(data.sources, data.current_source);
            model_helpers.fetch_sources(ractive);
            ractive.update();
        },
        next: function(ractive) {
            var data = ractive.data;
            data.current_source = helpers.get_next(data.sources, data.current_source);
            model_helpers.fetch_sources(ractive);
            ractive.update();
        }
    },

    ractive = Ractive.extend({
        template: '#frontpage',
        data: {
            sources: config.sources,
            current_source: 0,
            cols_in_image: 2,
            //expressions
            item_at_index: function(array, index) {
                return array[index];
            },
            button_class: function(index, current_feed) {
                if (index === current_feed) {
                    return 'primary';
                } else {
                    return 'info';
                }
            },
            format: function(item, type) {
                var val = parseInt(item.split(' ')[0], 10) || 0;
                return [val, type].join(' ');
            },
            cols_in_title: function(image) {
                if (image === undefined) {
                    return 10;
                } else {
                    return 10-this.data.cols_in_image;
                }
            }
        },
        beforeInit: function() {
            model_helpers.fetch_sources(this);
        },
        init: function() {
            this.on({
                update_feed: function(event) {
                    var
                        arr = event.keypath.split('.'),
                        index = this.data.current_source;
                    this.data.sources[index].current_feed = parseInt(arr[2], 10);
                    this.update();
                },
                mark_read: function(event) {
                    event.context.class = 'read';
                    this.update();
                    helpers.open_in_new_tab(event);
                },
                open_image: function(event) {
                    helpers.open_in_new_tab(event);
                }
            });
        }
    });

var ractive = new ractive({
    el: '#container'
});

$('#previous').click(event_handlers.previous.bind(this, ractive));
$('#next').click(event_handlers.next.bind(this, ractive));
$('body').keydown(function(e) {
    if(e.keyCode === 37) {
        event_handlers.previous(ractive);
    } else if(e.keyCode === 39) {
        event_handlers.next(ractive);
    }
});
